const mongoose = require('mongoose')

mongoose.connect(
    'mongodb+srv://api:3680@cluster0.faf83.mongodb.net/myFirstDatabase?retryWrites=true&w=majority',
    { useUnifiedTopology: true, useNewUrlParser: true, useFindAndModify: false },
    (err) => {
        if(err) return console.log(err)
    })

module.exports = mongoose