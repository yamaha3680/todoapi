const joi = require('joi')

const sheme = joi.object({
    login: joi.string()
        .min(4)
        .max(30)
        .pattern(new RegExp('^[a-zA-Z0-9]{4,30}$'))
        .required(),

    password: joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{8,30}$')),

    token: joi.string()
        .required()
})

module.exports = sheme