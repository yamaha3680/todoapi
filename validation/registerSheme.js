const joi = require('joi')

const sheme = joi.object({
    login: joi.string()
        .min(8)
        .max(30)
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$'))
        .required(),

    password: joi.string()
        .pattern(new RegExp('^[a-zA-Z0-9]{3,30}$')),

    repeadPassword: joi.ref('password')
})

module.exports = sheme