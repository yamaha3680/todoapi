const express = require('express')
const router = express.Router()
const userModel = require('../mongo/models/userModel')


router.get('/', async (req, res, next) => {
    const users = await userModel.find({}, {})
    res.json({
        'message': users
    })
});


module.exports = router
