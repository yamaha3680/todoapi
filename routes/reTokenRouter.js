const express = require('express')
const router = express.Router()
const jwt = require('jsonwebtoken')
const config = require('config')
const reTokenSheme = require('../validation/reTokenSheme')
const userModel = require('../mongo/models/userModel')

router.get('/', async (req, res, next) => {
    const {error, value} = reTokenSheme.validate(req.body)
    if (error) return res.status(400).json({'message': 'wrong arguments'})

    await jwt.verify(
        value.token,
        config.get('key'),
        async (err, decoded) => {
            if (err) return res.status(400).json({'message': 'invalid token'})

            const user = await userModel.findOne({
                login: value.login,
                password: value.password
            })

            if (!user) return res.status(400).json({'message': 'user is not found'})

            const token = jwt.sign({
                    payload: {
                        userId: user._id,
                        userLogin: user.login,
                    }
                },
                config.get('key'),
                {expiresIn: '3h'}
            )

            res.json({
                'message': 'ok',
                'token': token,
                'time': Date.now()
            })
        }
    )
})

module.exports = router
