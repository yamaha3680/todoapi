const express = require('express')
const router = express.Router()
const userModel = require('../mongo/models/userModel')
const registerSheme = require('../validation/registerSheme')

router.post('/', async (req, res, next) => {
    const { error, value } = registerSheme.validate(req.body)
    if (error) return res.status(400).json({'message': 'wrong arguments'})

    const user = new userModel({
        login: value.login,
        password: value.password
    })
    await user.save()
    res.json({
        "message": "ok"
    })
})

module.exports = router