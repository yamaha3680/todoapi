const express = require('express')
const router = express.Router()
const loginSheme = require('../validation/loginSheme')
const userModel = require('../mongo/models/userModel')
const jwt = require('jsonwebtoken')
const config = require('config')


router.get('/', async (req, res, next) => {
    const {error, value} = loginSheme.validate(req.body)
    if (error) return res.status(400).json({'message': 'wrong arguments'})

    const user = await userModel.findOne({
        login: value.login,
        password: value.password
    })

    if (!user) return res.status(400).json({'message': 'user is not found'})

    const token = jwt.sign({
            payload: {
                userId: user._id,
                userLogin: user.login,
            }
        },
        config.get('key'),
        {expiresIn: '3h'}
    )

    res.json({
        'message': 'ok',
        'token': token,
        'time': Date.now()
    })
})

module.exports = router